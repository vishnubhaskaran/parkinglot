package com.attinad.ticket;

import com.attinad.vehicle.Vehicle;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class NormalTicket extends Ticket {
    public NormalTicket(int slotNo, int floorNo, Vehicle vehicle, LocalDateTime now) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        this.timeOfEntry=now;
        this.vehicle=vehicle;
        this.floorNo=floorNo;
        this.slotNo=slotNo;
    }
}
