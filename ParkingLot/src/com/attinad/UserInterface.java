package com.attinad;

import java.util.Scanner;

public class UserInterface {

    public void userEntry(){
        String type,number;
        Scanner scanner=new Scanner(System.in);
        ParkingLot parkingLot=new ParkingLot();
        while(true) {
            System.out.println("Welcome to Parking Lot xyz");
            System.out.println("Enter the type of vehicle \n 1.Car \n 2. Bike\n");
            type = scanner.next();
            System.out.println("Enter the Vehicle No");
            number = scanner.next();
            parkingLot.generateTicket(type, number);
        }

    }

}
