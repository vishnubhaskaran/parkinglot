package com.attinad.slot;

import com.attinad.ParkingConstants;
import com.attinad.vehicle.Vehicle;

public abstract class Slot {
   protected String status;
   protected int slotNo;
   protected Vehicle vehicle;

   public int getSlotNo() {
      return slotNo;
   }

   public String getStatus() {
      return this.status;
   }

   public void occupy(){
      this.status=ParkingConstants.OCCUPIED;
   }
   public void release(){
      this.status=ParkingConstants.FREED;
   }
}
