package com.attinad;

import com.attinad.slot.Slot;
import com.attinad.ticket.NormalTicket;
import com.attinad.ticket.Ticket;
import com.attinad.vehicle.Vehicle;
import com.attinad.vehicle.VehicleFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ParkingLot {

    private Floor[] floors;
    private final int no_of_floors = 5;

    public ParkingLot() {
        floors=new Floor[no_of_floors];
        for (int i = 0; i < no_of_floors; i++) {
            floors[i] = new Floor(i, 30, 5);
        }
    }

    public void generateTicket(String type,String plate) {
        Slot freeSlot = null; int floorNo=0;
        Vehicle vehicle=VehicleFactory.createVehicle(type,plate);
        int flag = 0;
        for (int i = 0; i < no_of_floors; i++) {

            if (flag == 0) {
                freeSlot = floors[i].getFreeSlot();
                freeSlot.occupy();
                floorNo=i;
                flag = 1;
            }
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        LocalDateTime now = LocalDateTime.now();
        Ticket ticket= new NormalTicket(freeSlot.getSlotNo(),floorNo,vehicle,now);
        ticket.display();
    }
}
