package com.attinad.vehicle;

public abstract class Vehicle {

    protected String numberPlate;
    protected int noOfSlots;
    protected String Size;

    public void setNumber(String number){

        this.numberPlate=number;
    }

    public String getNumberPlate() {
        return numberPlate;
    }
}
