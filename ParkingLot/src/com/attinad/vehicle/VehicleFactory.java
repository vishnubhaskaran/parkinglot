package com.attinad.vehicle;

import com.attinad.ParkingConstants;

public class VehicleFactory {

    public static Vehicle createVehicle(String ch,String plate) {
        if (ch.equals(ParkingConstants.CAR)) {
            return new Car(plate);
        } else if (ch.equals(ParkingConstants.BIKE)) {
            return new Bike(plate);
        }
      return null;
    }
}

