package com.attinad;

import com.attinad.slot.HandicapSlot;
import com.attinad.slot.NormalSlot;
import com.attinad.slot.Slot;

import java.util.ArrayList;
import java.util.List;

public class Floor {
    Slot[] slot;
    Slot[] hSlot;
    private int floorNo;
    private int noOfSlots;
    private int noOfHandicappedSlots;
    private int noOfFreeSlots;

    Floor(int floorNo, int noOfSlots, int noOfHandicapSlots) {
         slot=new Slot[noOfSlots];
         hSlot=new Slot[noOfHandicapSlots];
        this.floorNo = floorNo;
        this.noOfSlots = noOfSlots;
        this.noOfHandicappedSlots = noOfHandicapSlots;
        for (int i = 0; i < noOfSlots; i++) {
            Slot slo=new NormalSlot(i);
            slot[i] =slo;
        }
        for (int i = 0; i < noOfHandicapSlots; i++) {
            hSlot[i] = new HandicapSlot(i);
        }
    }

    public Slot getFreeSlot() {
        for (int i = 0; i < this.noOfSlots; i++) {

            if (slot[i].getStatus().equals(ParkingConstants.FREED)) {
                return slot[i];

            }

        }
        return null;
    }
}
